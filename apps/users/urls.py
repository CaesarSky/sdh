from django.urls import path

from apps.users.views import IndexListView, UserProfileView, InvitationCodeGeneratorView


urlpatterns = [
    path('', IndexListView.as_view(), name='index'),
    path('inspiration-code/', InvitationCodeGeneratorView.as_view(), name='inspiration_code'),
    path('<slug:slug>/', UserProfileView.as_view(), name='profile'),
]
