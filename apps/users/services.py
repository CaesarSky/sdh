import uuid

from django.shortcuts import get_object_or_404
from django.db.models import F
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from django.template.loader import render_to_string

from django.core.mail import EmailMessage

from apps.users.models import CustomUser
from apps.users.tokens import account_activation_token

def save_invitation_code(user: CustomUser):
    user.invitation_code = str(uuid.uuid4())
    user.save(update_fields=['invitation_code'])


def send_email_message(user, request):
    current_site = get_current_site(request)
    mail_subject = 'Activate your blog account.'
    message = render_to_string('acc_active_email.html', {
        'user': user,
        'domain': current_site.domain,
        'uid':urlsafe_base64_encode(force_bytes(user.pk)),
        'token':account_activation_token.make_token(user),
    })
    to_email = user.email
    email = EmailMessage(
        mail_subject, message, to=[to_email]
    )
    email.send()

def signup_user(form, request):
    password = form.cleaned_data.get('password2')
    invitation_code = form.cleaned_data.get('invitation_code_user')
    user = form.save(commit=False)
    if invitation_code:
        who_registered = get_object_or_404(CustomUser, invitation_code=invitation_code)
        user.parent = who_registered
    user.set_password(password)
    user.is_active = False
    user.save()
    send_email_message(user, request)
    