from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import ugettext_lazy as _

from apps.users.models import CustomUser


@admin.register(CustomUser)
class CustomUserAdmin(UserAdmin):
    """
        Creating own admin and customise it to our own works
    """
    fieldsets = (
        (None, {'fields': ('username', 'password')},),
        (_('Персональная информация'), {'fields': ('first_name', 'last_name', 'email', 'slug')}),
        (_('Разрешения'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                      'groups', 'user_permissions')}),
        (_('Главные даты'), {'fields': ('last_login', 'date_joined')}),
        (_('Статус на сайте'), {'fields': ('is_verified', 'counter', 'points', 'invitation_code', 'parent')})
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', 'password1', 'password2'),
        }),
    )
    list_display = ('username', 'first_name', 'last_name', 'is_staff')
    search_fields = ('username', 'first_name', 'last_name')
    ordering = ('username',)
