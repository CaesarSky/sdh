from django import forms
from django.contrib.auth import get_user_model

from apps.users.models import CustomUser


User = get_user_model()

class SignupForm(forms.ModelForm):
    email = forms.EmailField(max_length=200)
    invitation_code_user = forms.CharField(max_length=40, required=False)
    password1 = forms.CharField(max_length=20, widget=forms.PasswordInput)
    password2 = forms.CharField(max_length=20, widget=forms.PasswordInput)
    class Meta:
        model = User
        fields = ('username', 'email', 'invitation_code_user', 'password1', 'password2')
    
    def clean(self):
        cleaned_data = super(SignupForm, self).clean()
        password = cleaned_data.get("password1")
        confirm_password = cleaned_data.get("password2")
        invitation_code = cleaned_data.get('invitation_code_user')

        if password != confirm_password:
            raise forms.ValidationError(
                "Ваши пароли не совпадают."
            )

        if counter := CustomUser.objects.last().counter:
            if  counter >= 5 and not invitation_code:
                raise forms.ValidationError(
                    "Пожалуйста, введите код-приглашения для регистрации."
                )
        return cleaned_data
