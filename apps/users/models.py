from django.contrib.auth import get_user_model
from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.models import AbstractUser
from django.contrib.auth.validators import UnicodeUsernameValidator
from django.db.models.signals import post_save
from django.utils.translation import gettext_lazy as _
from django.utils import timezone
from django.db import models
from django.core.exceptions import ValidationError


class CustomUserManager(BaseUserManager):
    """Define a model manager for User model with no username field."""

    def _create_user(self, username, password=None, **extra_fields):
        """Create and save a User with the given username and password."""
        if not username:
            raise ValueError('The given username must be set')
        user = self.model(username=username, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, username, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(username, password, **extra_fields)

    def create_superuser(self, username, password=None, **extra_fields):
        """Create and save a SuperUser with the given username and password."""
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(username, password, **extra_fields)


class CustomUser(AbstractUser):
    """
    Full Custom User model that extends from AbstractUser
    Filled all required fields
    """
    username_validator = UnicodeUsernameValidator()

    username = models.CharField(
        verbose_name=_('Имя пользователя'),
        max_length=150,
        unique=True,
        help_text=_('Обязателен. 150 символов или меньше. Буквы, числа и только @/./+/-/_ символы.'),
        validators=[username_validator],
        error_messages={
            'unique': _("Пользователь с таким именем уже существует."),
        },
    )
    first_name = models.CharField(verbose_name='Имя пользователя', blank=True, null=True, max_length=255)
    last_name = models.CharField(verbose_name='Фамилия пользователя', blank=True, null=True, max_length=255)
    email = models.EmailField(verbose_name='Почтовый адрес', unique=True)
    slug = models.SlugField(verbose_name='Ссылка на пользователя', unique=True)
    is_verified = models.BooleanField(verbose_name='Аккаунт активирован', default=False)
    counter = models.IntegerField(
        verbose_name='Зарегистрированный пользователь без кода-приглашения',
        blank=True, null=True
    )
    parent = models.ForeignKey(
        'self', verbose_name='Пригласил', on_delete=models.CASCADE, related_name='children',
        blank=True, null=True
    )
    points = models.IntegerField(verbose_name='Количество очков пользователя', default=1)
    invitation_code = models.CharField(verbose_name='Код-приглашения', max_length=40)
    is_staff = models.BooleanField(
        _('Статус персонала'),
        default=False,
        help_text=_('Определение, может ли пользователь войти на сайт администратора.'),
    )
    is_active = models.BooleanField(
        _('Активный'),
        default=True,
        help_text=_(
            'Определение, следует ли считать этого пользователя активным. '
            'Снимите этот флажок вместо удаления учетных записей.'
        ),
    )
    date_joined = models.DateTimeField(_('Дата добавления.'), default=timezone.now)
    EMAIL_FIELD = 'email'
    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = []

    objects = CustomUserManager()

    class Meta:
        verbose_name = _('Пользователь')
        verbose_name_plural = _('Пользователи')
    
    @property
    def all_parent_users(self):
        parents = []
        current_parent = self.parent
        while current_parent is not None:
            parents.append(current_parent)
            current_parent = current_parent.parent
        return parents

    def save(self, *args, **kwargs):
        if self.username:
            self.slug = self.username
        user = CustomUser.objects.last()
        if not user:
            self.counter = 1
        elif user:
            if not user.is_superuser:
                self.counter = user.counter + 1
            else:
                self.counter = 1
        super().save(*args, **kwargs)

    def __str__(self):
        return self.username
