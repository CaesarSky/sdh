from django.views.generic import ListView, DetailView, View, FormView
from django.http import HttpResponse
from django.contrib.auth import get_user_model
from django.shortcuts import redirect
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth import login
from django.utils.encoding import force_text
from django.utils.http import urlsafe_base64_decode
from django.db.models import F

from apps.users.forms import SignupForm
from apps.users.services import account_activation_token, save_invitation_code, signup_user
from apps.users.models import CustomUser


User = get_user_model()


class IndexListView(ListView):
    template_name: str = 'base.html'
    queryset = User.objects.order_by('points')
    context_object_name = 'top_users'



class UserProfileView(LoginRequiredMixin, DetailView):
    template_name: str = 'profile.html'
    model = User
    context_object_name = 'user'

    def get_object(self):
        return self.model.objects.get(slug=self.request.user.slug)


class InvitationCodeGeneratorView(View):
    def post(self, request, *args, **kwargs):
        save_invitation_code(request.user)
        return redirect('profile', slug=request.user.slug)



class SignUpView(FormView):
    form_class = SignupForm
    model = User
    template_name: str = 'registration/signup.html'
    success_url = '/index/'

    def form_valid(self, form):
        signup_user(form, self.request)
        return HttpResponse('Please confirm your email address to complete the registration')


class ActivateUserView(View):
    def post(self, request, uidb64, token):
        try:
            uid = force_text(urlsafe_base64_decode(uidb64))
            user = User.objects.get(pk=uid)
        except(TypeError, ValueError, OverflowError, User.DoesNotExist):
            user = None
        if user is not None and account_activation_token.check_token(user, token):
            user.is_active = True
            user.save()
            parent_users = []
            for user in user.all_parent_users:
                user.points = F('points') + 1
                parent_users.append(user)
            CustomUser.objects.bulk_update(parent_users, ['points'], batch_size=1000)
            login(request, user)
            return HttpResponse('Thank you for your email confirmation. Now you can login your account.')
        else:
            return HttpResponse('Activation link is invalid!')
